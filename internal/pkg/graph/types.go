package graph

type RealPoint struct {
	X, Y float64
}

type RealRectangle struct {
	XMax, YMax float64
}

type Point struct {
	X, Y int
}

type Rectangle struct {
	XMax, YMax int
}
