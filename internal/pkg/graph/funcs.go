package graph

import "math"

func PointToRealPoint(p Point, rect Rectangle, realRect RealRectangle) RealPoint {
	return RealPoint{
		X: float64(p.X) / float64(rect.XMax) * realRect.XMax,
		Y: realRect.YMax - float64(p.Y)/float64(rect.YMax)*realRect.YMax,
	}
}

func RealPointToPoint(p RealPoint, realRect RealRectangle, rect Rectangle) Point {
	return Point{
		X: int(p.X / realRect.XMax * float64(rect.XMax)),
		Y: rect.YMax - int(p.Y/realRect.YMax*float64(rect.YMax)),
	}
}

func Distance(a, b RealPoint) float64 {
	dx := a.X - b.X
	dy := a.Y - b.Y

	return math.Sqrt(dx*dx + dy*dy)
}
