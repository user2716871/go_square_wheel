package square

import (
	"math"
	"square_wheel/internal/pkg/graph"
)

type Square struct {
	Center graph.RealPoint
	Side   float64
	Angle  float64
}

const (
	epsilon = 0.001
)

// TODO: fix strange formula: square rotates other side

func (s *Square) Contains(point graph.RealPoint) bool {

	dist := graph.Distance(s.Center, point)
	if dist < s.Side/2 {
		return true
	}

	dx := point.X - s.Center.X
	dy := point.Y - s.Center.Y

	var pointAngle float64
	switch {
	case math.Abs(dx) < epsilon:
		pointAngle = math.Asin(dy / dist)
	case math.Abs(dy) < epsilon:
		pointAngle = math.Acos(dx / dist)
	default:
		pointAngle = math.Atan(dx / dy)
	}

	relativeAngle := pointAngle - s.Angle
	relativeX := dist * math.Cos(relativeAngle)
	relativeY := dist * math.Sin(relativeAngle)

	return math.Abs(relativeX) < s.Side/2 && math.Abs(relativeY) < s.Side/2
}
