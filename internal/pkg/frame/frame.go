package frame

import (
	"image"
	"image/color/palette"
	"square_wheel/internal/pkg/graph"
	"square_wheel/internal/pkg/mycolor"
	"square_wheel/internal/pkg/square"
)

type Frame struct {
	RealRect graph.RealRectangle
	Rect     graph.Rectangle
	Img      *image.Paletted
}

func NewFrame(width, height float64, pixelsPerMeter int) *Frame {

	realRect := graph.RealRectangle{
		XMax: width,
		YMax: height,
	}
	rect := graph.Rectangle{
		XMax: int(width * float64(pixelsPerMeter)),
		YMax: int(height * float64(pixelsPerMeter)),
	}
	img := image.NewPaletted(
		image.Rect(0, 0, rect.XMax, rect.YMax),
		palette.Plan9,
	)

	return &Frame{
		RealRect: realRect,
		Rect:     rect,
		Img:      img,
	}
}

func (f *Frame) DrawRotatedSquare(
	sqr square.Square,
	colorFunc mycolor.ColorForPoint,
) {
	for x := 0; x < f.Rect.XMax; x++ {
		for y := 0; y < f.Rect.YMax; y++ {
			realPoint := graph.PointToRealPoint(graph.Point{X: x, Y: y}, f.Rect, f.RealRect)
			if sqr.Contains(realPoint) {
				f.Img.Set(x, y, colorFunc(x, y))
			}
		}
	}
}
