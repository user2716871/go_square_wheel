package mycolor

import "image/color"

type ColorForPoint func(x, y int) color.RGBA
