package mycolor

import (
	"image/color"
	"math"
	"math/rand"
)

func Black(x, y int) color.RGBA {
	return color.RGBA{}
}

func Red(x, y int) color.RGBA {
	return color.RGBA{R: math.MaxUint8}
}

func Green(x, y int) color.RGBA {
	return color.RGBA{G: math.MaxUint8}
}

func Blue(x, y int) color.RGBA {
	return color.RGBA{B: math.MaxUint8}
}

func ColorNoise(x, y int) color.RGBA {

	getRandomByte := func() uint8 {
		return uint8(rand.Intn(math.MaxUint8 + 1))
	}

	return color.RGBA{
		R: getRandomByte(),
		G: getRandomByte(),
		B: getRandomByte(),
		A: getRandomByte(),
	}
}

func Chess(cellSize int) ColorForPoint {

	return func(x, y int) color.RGBA {
		x /= cellSize
		y /= cellSize
		if (x+y)%2 == 0 {
			return color.RGBA{
				R: math.MaxUint8,
				G: math.MaxUint8,
				B: math.MaxUint8,
				A: math.MaxUint8,
			}
		} else {
			return color.RGBA{}
		}
	}

}
