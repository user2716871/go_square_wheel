## go_square_wheel

Creates gif-pictures of rolling square wheel like this:

![alt text](docs/sample.gif)

### Command to get gif

```
make gif
```
