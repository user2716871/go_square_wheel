package main

import (
    "fmt"
    "image/gif"
    "os"
    "square_wheel/internal/pkg/frame"
    "square_wheel/internal/pkg/square"

    "square_wheel/internal/pkg/graph"
    "square_wheel/internal/pkg/mycolor"
)

const (
    horizontalSize float64 = 15
    verticalSize   float64 = 5
    pixelsPerMeter int     = 50
    squareSize     float64 = 2
)

const (
    speedMetersPerSec float64 = 2
    totalDistance     float64 = horizontalSize
    totalTime         float64 = totalDistance / speedMetersPerSec
    angularSpeed      float64 = speedMetersPerSec / (squareSize / 2)
)

const (
    framesPerSecond           = 10
    totalFrames               = int(framesPerSecond * totalTime)
    timeBetweenFrames float64 = 1.0 / framesPerSecond
    delayGif                  = int(timeBetweenFrames / 0.01)
)

// TODO: deal with formulas. smth go wrong
const (
    dX     = speedMetersPerSec * timeBetweenFrames
    dAlpha = angularSpeed * timeBetweenFrames * 0.75
)

func Background(sky, ground mycolor.ColorForPoint) *frame.Frame {

    fr := frame.NewFrame(horizontalSize, verticalSize, pixelsPerMeter)

    sqr := square.Square{
        Center: graph.RealPoint{X: 0, Y: verticalSize / 2},
        Side:   squareSize,
        Angle:  0,
    }

    for x := 0; x < fr.Rect.XMax; x++ {
        for y := 0; y < fr.Rect.YMax; y++ {
            if graph.PointToRealPoint(graph.Point{X: x, Y: y}, fr.Rect, fr.RealRect).Y < verticalSize/2 {
                fr.Img.Set(x, y, ground(x, y))
            } else {
                fr.Img.Set(x, y, sky(x, y))
            }
        }
    }

    for i := 0; i < totalFrames; i++ {
        fr.DrawRotatedSquare(sqr, sky)

        sqr.Center.X += dX
        sqr.Angle += dAlpha
    }

    return fr
}

func main() {

    background := Background(mycolor.ColorNoise, mycolor.Blue)
    anim := gif.GIF{}

    sqr := square.Square{
        Center: graph.RealPoint{X: 0, Y: verticalSize / 2},
        Side:   squareSize,
        Angle:  0,
    }

    for i := 0; i < totalFrames; i++ {

        curFrame := *background
        tmp := *(background.Img)
        curFrame.Img = &tmp
        curFrame.Img.Pix = make([]uint8, len(background.Img.Pix))
        copy(curFrame.Img.Pix, background.Img.Pix)

        curFrame.DrawRotatedSquare(sqr, mycolor.Chess(10))

        sqr.Center.X += dX
        sqr.Angle += dAlpha

        anim.Delay = append(anim.Delay, delayGif)
        anim.Image = append(anim.Image, curFrame.Img)
    }

    err := gif.EncodeAll(os.Stdout, &anim)
    if err != nil {
        fmt.Println(err)
    }
}
